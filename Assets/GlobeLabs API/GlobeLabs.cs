﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

namespace GlobeLabsAPI
{
	public class GlobeLabs : MonoBehaviour
    {
		public string RedirectURI {
			get { 
				return GlobeLabsSettings.RedirectUri;
			}
		}

		public Action<AuthResult> OnAuthorizeFinishedEvent;
		public Action<PaymentResult> OnChargeFinishedEvent;

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>
        /// The access token.
        /// </value>
		public string AccessToken { get; set; }

		public string SubscriberNumber { get; set; }

		/// <summary>
		/// Gets the authentication login URL.
		/// </summary>
		/// <returns></returns>
		public string GetAuthLoginUrl ()
		{
			if (string.IsNullOrEmpty (GlobeLabsSettings.ApplicationId))
			{
				throw new ArgumentException ("getauthloginurl - applicationId is required.");
			}

			return AuthService.Instance.GetLoginDialogUrl (GlobeLabsSettings.ApplicationId);
		}

		public void Authorize (string code) {
			StartCoroutine (AuthorizeCoroutine (code));
		}

		public void Charge (PaymentPayload payload) {
			StartCoroutine (ChargeCoroutine (payload));
		}

		private IEnumerator AuthorizeCoroutine (string code) {
			WWW www = AuthService.Instance.AuthorizeWWWBuilder (code, 
				GlobeLabsSettings.ApplicationId, 
				GlobeLabsSettings.ApplicationSecret);
			yield return www;

			AuthResult result = AuthService.Instance.AuthorizeWWWResultParser (www);
			this.AccessToken = AuthService.Instance.AccessToken;
			this.SubscriberNumber = AuthService.Instance.SubscriberNumber;

			if (OnAuthorizeFinishedEvent != null) {
				OnAuthorizeFinishedEvent.Invoke (result);
			}
		}

		private IEnumerator ChargeCoroutine (PaymentPayload payload) {
			if (string.IsNullOrEmpty(this.AccessToken))
			{
				throw new ArgumentException("charge - access token is required.");
			}

			WWW www = PaymentService.Instance.ChargeWWWBuilder (payload, this.AccessToken);
			yield return www;

			PaymentResult result = PaymentService.Instance.ChargeWWWResultParser (www);

			if (OnChargeFinishedEvent != null) {
				OnChargeFinishedEvent.Invoke (result);
			}
		}

		public string GetShortCodeSuffix () {
			return GlobeLabsSettings.ShortCode.Substring(Math.Max(0, GlobeLabsSettings.ShortCode.Length - 4));
		}
    }
}
