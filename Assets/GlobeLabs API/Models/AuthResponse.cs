﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlobeLabsAPI
{
    public class AuthResponse
    {

		//[JsonProperty("access_token")]
        public string AccessToken { get; set; }

		//[JsonProperty("subscriber_number")]
        public string SubscriberNumber { get; set; }

		public AuthResponse (Dictionary<string, object> dict) {
			AccessToken = dict ["access_token"].ToString ();
			SubscriberNumber = dict ["subscriber_number"].ToString ();
		}

		public AuthResponse () {
			AccessToken = String.Empty;
			SubscriberNumber = String.Empty;
		}
    }
}
