﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlobeLabsAPI
{

    public class PaymentPayload
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public string Number { get; set; }
        /// <summary>
        /// Gets or sets the reference code.
        /// </summary>
        /// <value>
        /// The reference code.
        /// </value>
        public string ReferenceCode { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public string Amount { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
    }

    public class ErrorResponse
    {
        public string Error;

		public ErrorResponse (Dictionary <string, object> dict) {
			Error = dict["error"].ToString ();
		}
    }

    public class PaymentResult
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public PaymentResponse Result { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public HttpStatus Status { get; set; }

		public PaymentResult () {
			Result = null;
			Status = new HttpStatus ();
		}
    }

    public class AuthResult
    {
		public AuthResult () {
			Result = new AuthResponse ();
			Status = new HttpStatus ();
		}

        public AuthResponse Result { get; set; }
        public HttpStatus Status { get; set; }
    }

    public class HttpStatus
    {
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
        public int StatusCode { get; set; }
        /// <summary>
        /// Gets or sets the status description.
        /// </summary>
        /// <value>
        /// The status description.
        /// </value>
        public string StatusDescription { get; set; }

		public HttpStatus () {
			StatusCode = 0;
			StatusDescription = String.Empty;
		}
    }
}
