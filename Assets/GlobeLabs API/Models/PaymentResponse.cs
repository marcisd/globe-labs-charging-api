﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlobeLabsAPI
{
    public class ChargingInformation
    {

		//[JsonProperty("amount")]
        public string Amount { get; set; }

		//[JsonProperty("currency")]
        public string Currency { get; set; }

		//[JsonProperty("description")]
        public string Description { get; set; }

		public ChargingInformation (Dictionary <string, object> dict) {
			Amount = dict ["amount"].ToString ();
			Currency = dict ["currency"].ToString ();
			Description = dict ["description"].ToString ();
		}
    }

    public class PaymentAmount
    {

		//[JsonProperty("chargingInformation")]
        public ChargingInformation ChargingInformation { get; set; }

		//[JsonProperty("totalAmountCharged")]
        public string TotalAmountCharged { get; set; }

		public PaymentAmount (Dictionary <string, object> dict) {
			ChargingInformation = new ChargingInformation ((Dictionary <string, object>) dict ["chargingInformation"]);
			TotalAmountCharged = dict ["totalAmountCharged"].ToString ();
		}
    }

    public class AmountTransaction
    {

		//[JsonProperty("endUserId")]
        public string EndUserId { get; set; }

		//[JsonProperty("paymentAmount")]
        public PaymentAmount PaymentAmount { get; set; }

		//[JsonProperty("referenceCode")]
        public string ReferenceCode { get; set; }

		//[JsonProperty("serverReferenceCode")]
        public string ServerReferenceCode { get; set; }

		//[JsonProperty("resourceURL")]
        public object ResourceURL { get; set; }

		public AmountTransaction (Dictionary <string, object> dict) {
			EndUserId = dict ["endUserId"].ToString ();
			PaymentAmount = new PaymentAmount ((Dictionary <string, object>) dict["paymentAmount"]);
			ReferenceCode = dict ["referenceCode"].ToString ();
			ServerReferenceCode = dict ["serverReferenceCode"].ToString ();
			ResourceURL = dict ["resourceURL"];
		}

    }

    public class PaymentResponse
    {

		//[JsonProperty("amountTransaction")]
        public AmountTransaction AmountTransaction { get; set; }

		public PaymentResponse (Dictionary <string, object> dict) {
			AmountTransaction = new AmountTransaction ((Dictionary <string, object>) dict["amountTransaction"]);
		}
    }
}
