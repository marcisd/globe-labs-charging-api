﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using MiniJSON;

namespace GlobeLabsAPI
{
    class PaymentService : GlobeLabsBase
    {

		private static PaymentService _instance = null;
		private PaymentService() { }
		public static PaymentService Instance
		{
			get { 
				if (_instance == null) {
					_instance =  new PaymentService ();
				}
				return _instance; 
			}
		}

		/// <summary>
		/// Builds a WWW request for the Payment API
		/// </summary>
		public WWW ChargeWWWBuilder (PaymentPayload payload, string accessToken) {

			string url = string.Format(Config.PAYMENT_ENDPOINT, Config.API_VERSION);

			var collection = new NameValueCollection
			{
				{ "amount", payload.Amount },
				{ "description", payload.Description },
				{ "endUserId", payload.Number },
				{ "referenceCode", payload.ReferenceCode},
				{ "access_token", accessToken},
				{ "transactionOperationStatus", Config.TRANSACTION_CHARGED }
			};

			return BuildPost (url, collection);
		}

		/// <summary>
		/// Parses the Payment API result
		/// </summary>
		public PaymentResult ChargeWWWResultParser (WWW www) {

			var result = new PaymentResult();

			if (String.IsNullOrEmpty (www.error)) {
				if (www.text.Contains(Config.ERROR))
				{
					var jsonResponseDict = (Dictionary <string, object>) Json.Deserialize (www.text);
					var jsonResponse = new ErrorResponse (jsonResponseDict);
					result.Result = null;
					result.Status.StatusDescription = jsonResponse.Error;
					result.Status.StatusCode = (int)HttpStatusCode.BadRequest;
				}
				else
				{
					var jsonResponseDict = (Dictionary <string, object>) Json.Deserialize (www.text);
					var jsonResponse = new PaymentResponse (jsonResponseDict);
					result.Result = jsonResponse;
					result.Status = null;
				}
			} else {
				result.Result = null;
				result.Status.StatusDescription = www.error;
				result.Status.StatusCode = (int)HttpStatusCode.InternalServerError;
			}

			return result;
		}

    }
}
