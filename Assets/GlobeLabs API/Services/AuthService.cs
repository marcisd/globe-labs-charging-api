﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using MiniJSON;

namespace GlobeLabsAPI 
{
    class AuthService : GlobeLabsBase
    {
		private static AuthService _instance = null;
		private AuthService() { }
		public static AuthService Instance
		{
			get { 
				if (_instance == null) {
					_instance =  new AuthService ();
				}
				return _instance; 
			}
		}

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>
        /// The access token.
        /// </value>
        public string AccessToken { get; set; }
        /// <summary>
        /// Gets or sets the subscriber number.
        /// </summary>
        /// <value>
        /// The subscriber number.
        /// </value>
        public string SubscriberNumber { get; set; }

        /// <summary>
        /// Gets the login dialog URL.
        /// </summary>
        /// <param name="appId">The application identifier.</param>
        /// <returns></returns>
        public string GetLoginDialogUrl(string appId)
        {
            return string.Format("{0}?app_id={1}", Config.DIALOG_OAUTH_ENDPOINT, appId);
        }

		/// <summary>
		/// Builds a WWW request for the Authorization API
		/// </summary>
		public WWW AuthorizeWWWBuilder (string code, string appId, string appSecret) {
			string url = Config.ACCESS_TOKEN_ENDPOINT;

			NameValueCollection parameter = new NameValueCollection();

			parameter.Add("app_id", appId);
			parameter.Add("app_secret", appSecret);
			parameter.Add("code", code);

			string postData = string.Empty;
			return BuildPost (url, parameter, Config.CONTENT_TYPE_FORM, postData);
		}

		/// <summary>
		/// Parses the Authorization API result
		/// </summary>
		public AuthResult AuthorizeWWWResultParser (WWW www) {

			var result = new AuthResult();
			this.AccessToken = string.Empty;
			this.SubscriberNumber = string.Empty;

			if (String.IsNullOrEmpty (www.error)) {
				if (www.text.Contains(Config.ERROR)) {
					var jsonResponseDict = (Dictionary <string, object>) Json.Deserialize (www.text);
					var jsonResponse = new ErrorResponse (jsonResponseDict);
					result.Result = null;
					result.Status.StatusDescription = jsonResponse.Error;
					result.Status.StatusCode = (int)HttpStatusCode.BadRequest;
				} else {
					var jsonResponseDict = (Dictionary <string, object>) Json.Deserialize (www.text);
					var jsonResponse = new AuthResponse (jsonResponseDict);
					result.Result = jsonResponse;
					result.Status = null;

					this.AccessToken = jsonResponse.AccessToken;
					this.SubscriberNumber = jsonResponse.SubscriberNumber;
				}
			} else {
				result.Result = null;
				result.Status.StatusDescription = www.error;
				result.Status.StatusCode = (int)HttpStatusCode.InternalServerError;
			}

			return result;
		}
    }
}
