﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

namespace GlobeLabsAPI
{
	internal abstract class GlobeLabsBase
    {
		private static string EscapeUrlFix (string str) {
			string esc = WWW.EscapeURL (str);
			return esc.Replace ("+", "%20");
		}

		protected WWW BuildPost (string requestUrl, NameValueCollection parameters, string contentType, string postData)
		{
			string url = requestUrl;
			if (parameters.Count > 0) {
				url += "?";
				url += String.Join ("&", parameters.AllKeys.Select (k => string.Format ("{0}={1}", k, EscapeUrlFix (parameters[k]))).Cast<string> ().ToArray ());
			}

			Dictionary<string, string> headers = new Dictionary<string, string> ();
			headers.Add ("Content-Type", contentType);

			byte[] bytes;
			if (String.IsNullOrEmpty (postData)) {
				// empty post data hack
				bytes = new byte[]{ 0 };
			} else {
				bytes = Encoding.UTF8.GetBytes(postData);
			}
			headers.Add ("Content-Length", String.Format ("{0}", bytes.Length));

			return new WWW (url, bytes, headers);
		}

		protected WWW BuildPost (string requestUrl, string contentType, string postData)
        {

			Dictionary<string, string> headers = new Dictionary<string, string> ();
			headers.Add ("Content-Type", contentType);

			byte[] bytes = Encoding.UTF8.GetBytes(postData);
			headers.Add ("Content-Length", String.Format ("{0}", bytes.Length));

			return new WWW (requestUrl, bytes, headers);
        }

		protected WWW BuildPost (string requestUrl, NameValueCollection collection)
        {
			WWWForm form = new WWWForm ();

			var items = collection.AllKeys.Select (k => new {key = k, value = collection[k]});
			items.ToList ().ForEach (item => form.AddField (item.key, item.value));

			return new WWW (requestUrl, form);
        }

		protected WWW BuildGet (string requestUrl, NameValueCollection parameters) {

			if (parameters.Count == 0) {
				return new WWW (requestUrl);
			}

			string paramStr = "?";
			paramStr += String.Join ("&", parameters.AllKeys.Select (k => string.Format ("{0}={1}", k, EscapeUrlFix (parameters[k]))).Cast<string> ().ToArray ());

			return new WWW (requestUrl + paramStr);

		}
    }
}
