﻿using UnityEngine;
using System.Collections;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GlobeLabsAPI {
	public class GlobeLabsSettings : ScriptableObject {

		private static readonly string GlobeLabsSettingsAssetName = "GlobeLabsSettings";

		private static GlobeLabsSettings s_instance;

		private static GlobeLabsSettings Instance
		{
			get
			{
				if (s_instance == null)
				{
					s_instance = Resources.Load (GlobeLabsSettingsAssetName) as GlobeLabsSettings;
					if (s_instance == null)
					{
						// If not found, autocreate the asset object.
						s_instance = ScriptableObject.CreateInstance<GlobeLabsSettings>();
						#if UNITY_EDITOR
						string properPath = Path.Combine(Application.dataPath, "Resources");
						if (!Directory.Exists(properPath))
						{
							AssetDatabase.CreateFolder("Assets", "Resources");
						}
							
						string fullPath = Path.Combine ("Assets/Resources", GlobeLabsSettingsAssetName + ".asset");

						AssetDatabase.CreateAsset(s_instance, fullPath);

						AssetDatabase.SaveAssets ();
						#endif
					}
				}

				return s_instance;
			}
		}

		[SerializeField] private string _applicationId;
		public static string ApplicationId {
			get { 
				return Instance._applicationId;
			}
			set { 
				if (Instance._applicationId != value) {
					Instance._applicationId = value;
					GlobeLabsSettings.DirtyEditor ();
				}
			}
		}

		[SerializeField] private string _applicationSecret;
		public static string ApplicationSecret {
			get { 
				return Instance._applicationSecret;
			}
			set { 
				if (Instance._applicationSecret != value) {
					Instance._applicationSecret = value;
					GlobeLabsSettings.DirtyEditor ();
				}
			}
		}

		[SerializeField] private string _shortCode;
		public static string ShortCode {
			get { 
				return Instance._shortCode;
			}
			set { 
				if (Instance._shortCode != value) {
					Instance._shortCode = value;
					GlobeLabsSettings.DirtyEditor ();
				}
			}
		}

		[SerializeField] private string _redirectUri;
		public static string RedirectUri {
			get { 
				return Instance._redirectUri;
			}
			set { 
				if (Instance._redirectUri != value) {
					Instance._redirectUri = value;
					GlobeLabsSettings.DirtyEditor ();
				}
			}
		}

		private static void DirtyEditor () {
			#if UNITY_EDITOR
			EditorUtility.SetDirty(Instance);
			#endif
		}

		[MenuItem ("GlobeLabs/Settings")]
		public static void Edit()
		{
			#if UNITY_EDITOR
			Selection.activeObject = Instance;
			#endif
		}
	}
}
