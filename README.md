# README #

Charge in-game purchases directly to the bill of a Globe or Touch Mobile subscriber. This library is based on the [GlobeLabs API for the .NET platform](https://github.com/globelabs/api/tree/master/NET).

### Prerequisite ###

* Sign up and create a GlobeLabs account at http://www.globelabs.com.ph/teaser .
* Create a new app in the dashboard. Make sure the Charging (PHP) API type is checked.

### Setting-up the Unity Library ###

* Download the GlobeLabsChargingAPI.unitypackage .
* In Unity, go to "Assets > Import Package > Custom Package..." and look for the downloaded file. Check everything and proceed with the import.
* A new menu item "GlobeLabs" will appear. Go to "GlobeLabs > Settings" and fill out the variables with the info from your GlobeLabs App.
* In your Scene, create a GameObject with and attach the "Globe Labs" Component.
* You can now access the "Authorize" and "Charge" methods via this GameObject.

For more information on how to use the GlobeLabs Charging API, refer to the [quick guide](https://docs.google.com/document/d/1G86orfgsONz9ALLByRfW_wx-xzR9n5XhW2mnfvJX_Hg/pub).